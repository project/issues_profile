# Arabic translation of Share Buttons by AddToAny (7.x-4.9)
# Copyright (c) 2015 by the Arabic translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Share Buttons by AddToAny (7.x-4.9)\n"
"POT-Creation-Date: 2015-07-28 07:34+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Arabic\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=((n==1)?(0):((n==0)?(1):((n==2)?(2):((((n%100)>=3)&&((n%100)<=10))?(3):((((n%100)>=11)&&((n%100)<=99))?(4):5)))));\n"

msgid "Content"
msgstr "المحتوى"
