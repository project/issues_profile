# French translation of Commentsblock (7.x-2.5)
# Copyright (c) 2014 by the French translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Commentsblock (7.x-2.5)\n"
"POT-Creation-Date: 2014-04-29 19:12+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: French\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n>1);\n"

msgid "Block"
msgstr "Bloc"
msgid "Info"
msgstr "Info"
msgid "Theme Tools"
msgstr "Outils de thème"
msgid "Master"
msgstr "Maître"
